use strict;

#open from JHannah 
open my $fh, "<:encoding(utf8)", "in.csv" or die "in.csv: $!";

my @headers;
#Could read headears as a hash but it probably needs to be an array.
#in order to read back in order?
#%headers = split(/,/ , readline($fh));
@headers = split(/,/ , readline($fh));
foreach(@headers)
{
  print("$_\n");
}

my @csv;  #csv will be an array of hashes.  
my $i = 0; my $h=0;
while(<$fh>) #Yes, reading into an array at once or Tie::File or CSV would be more efficient.
{
  chomp;
  #Split the $_ line from stdin and put each value into the corresponding hash key.
  #perldoc -f split did not have great examples.
  #DO NOT DO THIS if commas are part of data, not just delimiters.  See `perldoc -q split`.
  #`perldoc Text::CSV` is the way to go, not this.
  ( $csv[$i]{name}, $csv[$i]{age}, $csv[$i]{gender}, $csv[$i]{zip} ) = split(/,/ , $_, 1 + $#headers); 
  $i++;
}
my $NumRecords = $i;

for($i=0; $i<$NumRecords; $i++)
{
  printf(
    "name=$csv[$i]{name}\nage=$csv[$i]{age}\ngender=$csv[$i]{gender}\nzip=$csv[$i]{zip}\n\n" 
  );
}

#Print an array of hashes.  But for some reason, there is an off-by-one or ....?
for($i=0; $i<$NumRecords; $i++)
{
  print("\n");
  for($h=0; $h <= 3; $h++)
  {
    print("$headers[$h]=$csv[$i]{$headers[$h]}\n"); 
  }
}
