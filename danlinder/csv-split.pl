use strict;

open my $fh, "<:encoding(utf8)", "in.csv" or die "in.csv: $!";

my @headers =  split( /,/ , readline($fh) ) ;   # array ref
printf("Sanity Check: Lets print out number of headers and the headers themselves.\n")
printf("at dollar pound headers=%d\n", $#headers);
printf(@headers);
printf("\nforeach method:\n");
foreach(@headers)
{
  print "$_\n";
}

printf("The following has an off-by-one error or something?  Maybe it is readline()?\n");
while (my @row =  split( /,/ , readline($fh)) ) {
   for (my $i = 0; $i <= $#headers; $i++) {
      printf ("%s=%s\n", $headers[$i], $row[$i]);
   }
   printf("\n");
}


